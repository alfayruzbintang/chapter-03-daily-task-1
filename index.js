var readline = require('readline');
var pertambahan = require('./pertambahan.js');
var pengurangan = require('./pengurangan.js');
const hasilPerkalian = require('./heri')
const hasilPembagian = require('./pembagian.js')
const printPangkat = require("./perpangkatan/bilangan1.js");
const printPangkat2 = require("./perpangkatan/bilangan2.js");

var rl = readline.createInterface(process.stdin, process.stdout);

rl.question('Silakan masukan angka yang hendak ditambahkan, (contoh: 1,2,3) :', (answer1) => {
    let hasil = pertambahan(answer1)
    console.log(hasil) 
    rl.question('Silakan masukan angka yang hendak dikurangkankan lengkap dengan operator - ', (answer2) => {
        let hasil = pengurangan(answer2)
        console.log(hasil) 
        rl.question('ingin mengalikan angka berapa? ', (jawabanpertama) => {
            rl.question('dikalikan dengan angka berapa?  ', (jawabankedua) => {
                const angkaPerkalian = hasilPerkalian(parseInt(jawabanpertama),parseInt(jawabankedua));
                console.log("hasilnya = "+ angkaPerkalian)
                rl.question('ingin membagi angka berapa? ', (jawabanpembagianpertama) => {
                    rl.question('dibagi dengan angka berapa?  ', (jawabanpembagiankedua) => {
                        const angkaPembagian = hasilPembagian(parseInt(jawabanpembagianpertama),parseInt(jawabanpembagiankedua));
                        console.log("hasilnya = "+ angkaPembagian)
                        rl.question("Masukkan bilangan pertama : ", (bilangan1) => {
                            const hasilPrintBilangan1 = printPangkat(bilangan1);
                            console.log(hasilPrintBilangan1);
                        
                            rl.question("Masukkan bilangan kedua : ", (bilangan2) => {
                            const hasilPrintBilangan2 = printPangkat2(bilangan2);
                            console.log(hasilPrintBilangan2);
                        
                            // Perhitungan pangkat menggunakan fungsi Math.pow
                            const jumlah = Math.pow(bilangan1, bilangan2);
                            console.log(bilangan1 + " pangkat " + bilangan2 + " = " + jumlah);
                            rl.close();
                            });
                        });    
                    });
                });
            });
        });
    });
});

